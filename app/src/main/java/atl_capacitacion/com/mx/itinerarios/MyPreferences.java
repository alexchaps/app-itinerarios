package atl_capacitacion.com.mx.itinerarios;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ATL_DESARROLLO1 on 27/03/2017.
 */
public class MyPreferences {
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    Context _context;

    private static final int PRIVATE_MODE = 0;
    private static final String PREF_NAME="DailyFortune";
    private static final String IS_FIRSTIME="IsFirstTime";
    private static final String UserName = "name";

    public MyPreferences(Context context){
        this._context = context;
        pref=_context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor = pref.edit();
    }

    public boolean isFirstTime(){
        return pref.getBoolean(IS_FIRSTIME,true);
    }

    public void setOld(boolean b){
        if(b){
            editor.putBoolean(IS_FIRSTIME,false);
            editor.commit();
        }
    }

    public String getUserName(){
        return pref.getString(UserName,"");
    }

    public void setUserName(String name){
        editor.putString(UserName,name);
        editor.commit();
    }

}
