package atl_capacitacion.com.mx.itinerarios;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by ATL_DESARROLLO1 on 27/03/2017.
 */
public class ConnectionDetector {
    private Context context;

    ConnectionDetector(Context ctx){
        this.context=ctx;
    }

    public boolean isConnectingToInternet(){
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
        return activeNetworkInfo!= null && activeNetworkInfo.isConnected();
    }
}
